# Présentation

G1pourboire est un outil permettant de générer des pourboires en Ğ1.

En phase de développement, ceci est une version Alpha.

# Installation

Le logiciel n'est pas encore packagé, vous devez l'installer à la main.

## Installation des dépendances

- `python3`
- `pip3`
- `git`

### Debian :

```
$ sudo apt-get install python3 python3-pip git
```

### Arch Linux

```
$ sudo pacman -S python python-pip git
```

## Installation de G1pourboire

```
$ git clone https://git.duniter.org/Darks/g1pourboire.git
$ cd g1pourboire
$ pip3 install -r requirements.txt
```

# Utilisation

Actuellement le logiciel s'exécute en ligne de commande :

```
$ ./main.py <pages>
```

Les pourboires générés sont stockés dans `~/Documents/G1pourboire/`. Deux fichiers sont créés :

- PDF : fichier à imprimer puis découper
- Json : sauvegarde des identifiants de comptes

**Attention !** Actuellement, les fichiers de sortie ont pour nom la date et l'heure courante, à la minute près. Il n'y a pas de vérification si un fichier du même nom existe. Cela veut dire qu'une seconde génération dans la même minute écrasera la génération précédente.

# TODO

- Inteface graphique
- Provisionnement automatique
- Suivi des pourboires générés
- Récupération du solde des pourboires expirés
- Paquet stand-alone pour Windows
