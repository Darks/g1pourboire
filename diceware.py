# Generate a pseudo diceware password
# DO NOT USE IT FOR STRONG CRYPTOGRAPHIC PURPOSE!

from random import choice

def diceware(N, wordlist="wordlist.txt", separator="", camelcase=True):
	with open(wordlist, "r") as f:
		words = f.read().split('\n')
		out = []
		for i in range(N):
			word = choice(words)
			if camelcase:
				word = word.capitalize()
			out.append(word)
		return separator.join(out)
