from PIL import Image, ImageFont, ImageDraw
from reportlab.pdfgen import canvas
from reportlab.lib.units import cm
from duniterpy.key import SigningKey
from datetime import datetime
import os
import json
import qrcode

from diceware import diceware

class Generator():
	"""Generate some wallets, create the pdf to print"""
	def __init__(self, pages=1):
		self.pages = pages
		self.folder = os.path.expanduser('~/Documents/G1pourboire/')
		self.output = self.folder \
			+ datetime.now().strftime("G1pourboire_%Y-%m-%dT%Hh%M")
		self.wallets = []
		self.c = None

		# Create the folder
		if not os.path.exists(self.folder):
			os.makedirs(self.folder)

	def generate(self):
		for i in range(self.pages):
			for j in range(6): # 6 wallets per pages
				self.new_wallet()

		self.make_pdf()
		self.save_json()

	def new_wallet(self):
		# Generating credetials
		salt = diceware(3, separator="-", camelcase=False)
		password = diceware(3, separator="-", camelcase=False)

		# Generating public key from credentials
		key = SigningKey.from_credentials(salt, password)
		
		# Url to redirect to in the public QR code
		account_url = "https://g1.duniter.fr/#/app/wot/tx/" + key.pubkey + "/"

		# Generating wif data
		key.save_wif_file("privatekey.wif")
		wif_data = open("privatekey.wif").readlines()[-1].split(": ")[1]
		os.remove("privatekey.wif")

		# Creating the QR codes
		qr_pub = qrcode.make(account_url)
		qr_priv = qrcode.make(wif_data)

		# Open images
		recto = Image.open('recto.png')
		verso = Image.open('verso.png')

		# Pasting QR codes
		recto.paste(qr_pub.resize((200, 200)), (435, 36))
		verso.paste(qr_priv.resize((180, 180)), (580, 6))

		# Setting font
		font = ImageFont.truetype("Roboto-Medium.ttf", 18)

		# Writing public key
		draw = ImageDraw.Draw(recto)
		txt = "\n".join([key.pubkey[i:i+10] for i in range(0, len(key.pubkey), 10)])
		draw.text((665, 80), txt, (0,0,0), font=font)

		# Writing private keys
		draw = ImageDraw.Draw(verso)
		draw.text((570, 230), "ID : "+salt, (0,0,0), font=font)
		draw.text((570, 250), "PW : "+password, (0,0,0), font=font)
		
		# Add data to wallets
		self.wallets.append({
			'salt': salt,
			'password': password,
			'pubkey': key.pubkey,
			'date': 0,
			'recto': recto,
			'verso': verso,
			'wif': wif_data,
		})

	def gen_page(self, wallets):
		# Create a new canvas
		if self.c == None:
			self.c = canvas.Canvas(self.output + '.pdf')

		# Size of wallets. You may not edit those values until you know what you do
		width = 19*cm
		height = width*302/1270

		# Print recto
		for i, w in enumerate(wallets):
			self.c.drawInlineImage(w['recto'], (21*cm-width)/2, 24.2*cm-i*height, width, height)
		self.c.showPage()
		# And verso
		for i, w in enumerate(wallets):
			self.c.drawInlineImage(w['verso'], (21*cm-width)/2, 24.2*cm-i*height, width, height)
		self.c.showPage()
	
	def make_pdf(self):
		def chunks(l, n):
			"""Yield successive n-sized chunks from l."""
			for i in range(0, len(l), n):
				yield l[i:i + n]
		# Create pages
		for wallets in chunks(self.wallets, 6):
			self.gen_page(wallets)
		self.c.save()

	def save_json(self):
		data = [{
			'pubkey': w['pubkey'],
			'salt': w['salt'],
			'password': w['password']
		} for w in self.wallets]

		with open(self.output + ".json", 'w') as f:
			f.write(json.dumps(data, indent=4))
