#!/usr/bin/env python3
from generator import Generator
from sys import argv

g = Generator(int(argv[1]))
g.generate()
